#!/usr/bin/env python

# local library
import coloreffect

try:
    inkex.localize()
except:
    import gettext
    _ = gettext.gettext

class C(coloreffect.ColorEffect):
  def __init__(self):
    coloreffect.ColorEffect.__init__(self)
    self.OptionParser.add_option("-f", "--from_color",
        action="store", type="int", 
        dest="from_color", default="1364325887", 
        help="Replace color")
    self.OptionParser.add_option("-t", "--to_color",
        action="store", type="int",
        dest="to_color", default="1364325887",
        help="By color")
    self.OptionParser.add_option("--tab",
        action="store", type="string",
        dest="tab",
        help="The selected UI-tab when OK was pressed")

  def colmod(self,r,g,b):
    this_color = '%02x%02x%02x' % (r, g, b)

    fr_r = ((self.options.from_color >> 24) & 255)
    fr_g = ((self.options.from_color >> 16) & 255)
    fr_b = ((self.options.from_color >>  8) & 255)
    fr = '%02x%02x%02x' % (fr_r, fr_g, fr_b)

    to_r = ((self.options.to_color >> 24) & 255)
    to_g = ((self.options.to_color >> 16) & 255)
    to_b = ((self.options.to_color >>  8) & 255)
    to = '%02x%02x%02x' % (to_r, to_g, to_b)

    #inkex.debug(this_color+"|"+fr+"|"+to)
    if this_color == fr:
      return to
    else:
      return this_color

c = C()
c.affect()
