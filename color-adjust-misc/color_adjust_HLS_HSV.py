#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: Adjust HLS, HSV
"""

# standard library
import colorsys
import math

# local library
import coloreffect
import inkex


def clamp(minimum, x, maximum):
    """
    Clamp value to min/max values
    """
    return max(minimum, min(x, maximum))


class C(coloreffect.ColorEffect):
    """
    Inkscape Coloreffect extension module
    """
    def __init__(self):
        coloreffect.ColorEffect.__init__(self)
        self.OptionParser.add_option("--hls_hue",
                                     action="store", type="int",
                                     dest="hls_hue", default="0",
                                     help="Adjust HLS hue")
        self.OptionParser.add_option("--hls_lightness",
                                     action="store", type="int",
                                     dest="hls_lightness", default="0",
                                     help="Adjust HLS lightness")
        self.OptionParser.add_option("--hls_saturation",
                                     action="store", type="int",
                                     dest="hls_saturation", default="0",
                                     help="Adjust HLS saturation")
        self.OptionParser.add_option("--hsv_hue",
                                     action="store", type="int",
                                     dest="hsv_hue", default="0",
                                     help="Adjust HSV hue")
        self.OptionParser.add_option("--hsv_saturation",
                                     action="store", type="int",
                                     dest="hsv_saturation", default="0",
                                     help="Adjust HSV saturation")
        self.OptionParser.add_option("--hsv_value",
                                     action="store", type="int",
                                     dest="hsv_value", default="0",
                                     help="Adjust HSV value")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab when OK was pressed")

    def colmod(self, r, g, b):
        """
        Parse options (depending on active tab) and transform color values
        Return RGB string (hex)
        """
        # assign initial new value (identity):
        r_new, g_new, b_new = r, g, b

        if self.options.tab == '"hls_tab"':

            # based on Python's colorsys module:
            # <http://docs.python.org/2/library/colorsys.html>

            hls = list(colorsys.rgb_to_hls(r/255.0, g/255.0, b/255.0))
            hls_hueval = hls_lightval = hls_satval = 0

            if self.options.hls_hue:
                hls_hueval = hls[0] + self.options.hls_hue/360.0
                hls[0] = hls_hueval % 1
            if self.options.hls_lightness:
                hls_lightval = hls[1] + self.options.hls_lightness/100.0
                hls[1] = clamp(0.0, hls_lightval, 1.0)
            if self.options.hls_saturation:
                hls_satval = hls[2] + self.options.hls_saturation/100.0
                hls[2] = clamp(0.0, hls_satval, 1.0)

            rgb = colorsys.hls_to_rgb(hls[0], hls[1], hls[2])

            # return
            if hls_hueval or hls_lightval or hls_satval:
                r_new, g_new, b_new = [i*255 for i in rgb]

        elif self.options.tab == '"hsv_tab"':

            # based on Python's colorsys module:
            # <http://docs.python.org/2/library/colorsys.html>

            hsv = list(colorsys.rgb_to_hsv(r/255.0, g/255.0, b/255.0))
            hsv_hueval = hsv_satval = hsv_lightval = 0

            if self.options.hsv_hue:
                hsv_hueval = hsv[0] + self.options.hsv_hue/360.0
                hsv[0] = hsv_hueval % 1
            if self.options.hsv_saturation:
                hsv_satval = hsv[1] + self.options.hsv_saturation/100.0
                hsv[1] = clamp(0.0, hsv_satval, 1.0)
            if self.options.hsv_value:
                hsv_lightval = hsv[2] + self.options.hsv_value/100.0
                hsv[2] = clamp(0.0, hsv_lightval, 1.0)

            rgb = colorsys.hsv_to_rgb(hsv[0], hsv[1], hsv[2])

            # return
            if hsv_hueval or hsv_satval or hsv_lightval:
                r_new, g_new, b_new = [i*255 for i in rgb]

        else:   # catch remaining tabs and options on main page

            # 'About' tab, or unknown tab
            pass

        # return RGB hex string for new color values:
        return '%02x%02x%02x' % (r_new, g_new, b_new)


c = C()
c.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
