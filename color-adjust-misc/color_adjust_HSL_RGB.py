#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: Adjust HSL, RGB
"""

# standard library
import colorsys
import math

# local library
import coloreffect
import inkex


def clamp(minimum, x, maximum):
    """
    Clamp value to min/max values
    """
    return max(minimum, min(x, maximum))


class C(coloreffect.ColorEffect):
    """
    Inkscape Coloreffect extension module
    """
    def __init__(self):
        coloreffect.ColorEffect.__init__(self)
        self.OptionParser.add_option("--hsl_hue",
                                     action="store", type="int",
                                     dest="hsl_hue", default="0",
                                     help="Adjust HSL hue")
        self.OptionParser.add_option("--hsl_lightness",
                                     action="store", type="int",
                                     dest="hsl_lightness", default="0",
                                     help="Adjust HSL lightness")
        self.OptionParser.add_option("--hsl_saturation",
                                     action="store", type="int",
                                     dest="hsl_saturation", default="0",
                                     help="Adjust HSL saturation")
        self.OptionParser.add_option("--rgb_r",
                                     action="store", type="int",
                                     dest="rgb_r", default="0",
                                     help="Adjust RGB red value")
        self.OptionParser.add_option("--rgb_g",
                                     action="store", type="int",
                                     dest="rgb_g", default="0",
                                     help="Adjust RGB green value")
        self.OptionParser.add_option("--rgb_b",
                                     action="store", type="int",
                                     dest="rgb_b", default="0",
                                     help="Adjust RGB blue value")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab when OK was pressed")

    def colmod(self, r, g, b):
        """
        Parse options (depending on active tab) and transform color values
        Return RGB string (hex)
        """
        # assign initial new value (identity):
        r_new, g_new, b_new = r, g, b

        if self.options.tab == '"hsl_tab"':

            # based on Inkscape's color extension module
            # <http://bazaar.launchpad.net/~inkscape.dev/inkscape/trunk/view/head:/share/extensions/coloreffect.py>

            hsl = list(self.rgb_to_hsl(r/255.0, g/255.0, b/255.0))
            hsl_hueval = hsl_satval = hsl_lightval = 0

            if self.options.hsl_hue:
                hsl_hueval = hsl[0] + (self.options.hsl_hue / 360.0)
                hsl[0] = hsl_hueval % 1
            if self.options.hsl_saturation:
                hsl_satval = hsl[1] + (self.options.hsl_saturation / 100.0)
                hsl[1] = clamp(0.0, hsl_satval, 1.0)
            if self.options.hsl_lightness:
                hsl_lightval = hsl[2] + (self.options.hsl_lightness / 100.0)
                hsl[2] = clamp(0.0, hsl_lightval, 1.0)

            rgb = self.hsl_to_rgb(hsl[0], hsl[1], hsl[2])

            # return
            if hsl_hueval or hsl_satval or hsl_lightval:
                r_new, g_new, b_new = [i*255 for i in rgb]

        elif self.options.tab == '"rgb_tab"':

            if self.options.rgb_r != 0:
                r_new = clamp(0, r + self.options.rgb_r, 255)
            if self.options.rgb_g != 0:
                g_new = clamp(0, g + self.options.rgb_g, 255)
            if self.options.rgb_b != 0:
                b_new = clamp(0, b + self.options.rgb_b, 255)

        else:   # catch remaining tabs and options on main page

            # 'About' tab, or unknown tab
            pass

        # return RGB hex string for new color values:
        return '%02x%02x%02x' % (r_new, g_new, b_new)


c = C()
c.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
