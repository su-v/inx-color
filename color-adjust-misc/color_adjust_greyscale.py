#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: Adjust Greyscale
"""

# local library
from colormatrix import *
import coloreffect
import inkex


class C(coloreffect.ColorEffect):
    """
    Inkscape Coloreffect extension module
    """
    def __init__(self):
        coloreffect.ColorEffect.__init__(self)
        self.OptionParser.add_option("--grsc_red",
                                     action="store", type="float",
                                     dest="grsc_red", default="0.21",
                                     help="Greyscale level of R component")
        self.OptionParser.add_option("--grsc_blue",
                                     action="store", type="float",
                                     dest="grsc_blue", default="0.72",
                                     help="Greyscale level of G component")
        self.OptionParser.add_option("--grsc_green",
                                     action="store", type="float",
                                     dest="grsc_green", default="0.072",
                                     help="Greyscale level of B component")
        self.OptionParser.add_option("--grsc_strength",
                                     action="store", type="float",
                                     dest="grsc_strength", default="0.0",
                                     help="Greyscale strength level")
#        self.OptionParser.add_option("--grsc_transparent",
#                                     action="store", type="inkbool",
#                                     dest="grsc_transparent", default=False,
#                                     help="Greyscale, Transparent")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab when OK was pressed")

    def colmod(self, r, g, b):
        """
        Parse options (depending on active tab) and transform color values
        Return RGB string (hex)
        """
        # assign initial new value (identity):
        r_new, g_new, b_new = r, g, b

        if self.options.tab == '"grsc_tab"':

            # based on Inkscape 0.49 CPF 'Filters > Greyscale'

            # /**
            #     \brief    Custom predefined Greyscale filter.
            #
            #     Customize greyscale components.
            #
            #     Filter's parameters:
            #     * Red (-10.->10., default .21) -> colorMatrix (values)
            #     * Green (-10.->10., default .72) -> colorMatrix (values)
            #     * Blue (-10.->10., default .072) -> colorMatrix (values)
            #     * Lightness (-10.->10., default 0.) -> colorMatrix (values)
            #     * Transparent (boolean, default false) -> matrix structure
            #
            #     Matrix:
            #      normal       transparency
            #     R G B St 0    0 0 0 0    0
            #     R G B St 0    0 0 0 0    0
            #     R G B St 0    0 0 0 0    0
            #     0 0 0 1  0    R G B 1-St 0
            # */

            # gchar const *
            # Greyscale::get_filter_text (Inkscape::Extension::Extension * ext)
            # {
            #     if (_filter != NULL) g_free((void *)_filter);
            #
            #     std::ostringstream red;
            #     std::ostringstream green;
            #     std::ostringstream blue;
            #     std::ostringstream strength;
            #     std::ostringstream redt;
            #     std::ostringstream greent;
            #     std::ostringstream bluet;
            #     std::ostringstream strengtht;
            #     std::ostringstream transparency;
            #     std::ostringstream line;
            #
            #     red << ext->get_param_float("red");
            #     green << ext->get_param_float("green");
            #     blue << ext->get_param_float("blue");
            #     strength << ext->get_param_float("strength");
            #
            #     redt << - ext->get_param_float("red");
            #     greent << - ext->get_param_float("green");
            #     bluet << - ext->get_param_float("blue");
            #     strengtht << 1 - ext->get_param_float("strength");
            #
            #     if (ext->get_param_bool("transparent")) {
            #         line << "0 0 0 0";
            #         transparency << redt.str().c_str() << " " <<  greent.str().c_str() << " " <<  bluet.str().c_str() << " " << strengtht.str().c_str();
            #     } else {
            #         line << red.str().c_str() << " " <<  green.str().c_str() << " " <<  blue.str().c_str() << " " << strength.str().c_str();
            #         transparency << "0 0 0 1";
            #     }
            #
            #     _filter = g_strdup_printf(
            #         "<filter xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" style=\"color-interpolation-filters:sRGB;\" inkscape:label=\"Greyscale\">\n"
            #           "<feColorMatrix values=\"%s 0 %s 0 %s 0 %s 0 \" />\n"
            #         "</filter>\n", line.str().c_str(), line.str().c_str(), line.str().c_str(), transparency.str().c_str() );
            #     return _filter;
            # }; /* Greyscale filter */

            if True:  # always modifies colors

                red = self.options.grsc_red
                green = self.options.grsc_green
                blue = self.options.grsc_blue
                strength = self.options.grsc_strength

                red_t = -1 * self.options.grsc_red
                green_t = -1 * self.options.grsc_green
                blue_t = -1 * self.options.grsc_blue
                strength_t = 1.0 - self.options.grsc_strength

                # color extensions based on coloreffect.py don't modify opacity
                self.options.grsc_transparent = False

                grsc_matrix = [[0 for row in range(5)] for col in range(5)]
                for i in (3, 4):
                    grsc_matrix[i][i] = 1.0
                if self.options.grsc_transparent is False:
                    for i in range(3):
                            grsc_matrix[i][0] = red
                            grsc_matrix[i][1] = green
                            grsc_matrix[i][2] = blue
                            grsc_matrix[i][3] = strength
                else:
                    grsc_matrix[3][0] = red_t
                    grsc_matrix[3][1] = green_t
                    grsc_matrix[3][2] = blue_t
                    grsc_matrix[3][3] = strength_t

                #inkex.debug(grsc_matrix)

                # color as matrix
                rgba_matrix = rgba2matrix(r, g, b, 1)

                # multiply the two matrices
                rgba_matrix_new = matrixmult(grsc_matrix, rgba_matrix)

                # return
                if rgba_matrix_new:
                    r_new, g_new, b_new = matrix2rgb(rgba_matrix_new)
                else:
                    # silently fail (matrix multiplication likely failed)
                    pass

        else:   # catch remaining tabs and options on main page

            # 'About' tab, or unknown tab
            pass

        # return RGB hex string for new color values:
        return '%02x%02x%02x' % (r_new, g_new, b_new)


c = C()
c.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
