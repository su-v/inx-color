#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: Adjust Color Shift
"""

# local library
from colormatrix import *
import coloreffect
import inkex


class C(coloreffect.ColorEffect):
    """
    Inkscape Coloreffect extension module
    """
    def __init__(self):
        coloreffect.ColorEffect.__init__(self)
        self.OptionParser.add_option("--shift_shift",
                                     action="store", type="int",
                                     dest="shift_shift", default="330",
                                     help="Shift")
        self.OptionParser.add_option("--shift_sat",
                                     action="store", type="float",
                                     dest="shift_sat", default="0.6",
                                     help="Saturation")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab when OK was pressed")

    def colmod(self, r, g, b):
        """
        Parse options (depending on active tab) and transform color values
        Return RGB string (hex)
        """
        # assign initial new value (identity):
        r_new, g_new, b_new = r, g, b

        if self.options.tab == '"shift_tab"':

            # based on Inkscape 0.49 CPF 'Filters > Greyscale'

            # /**
            #     \brief    Custom predefined Color shift filter.
            #
            #     Rotate and desaturate hue
            #
            #     Filter's parameters:
            #     * Shift (0->360, default 330) -> color1 (values)
            #     * Saturation (0.->1., default 0.6) -> color2 (values)
            # */
            #
            # gchar const *
            # ColorShift::get_filter_text (Inkscape::Extension::Extension * ext)
            # {
            #     if (_filter != NULL) g_free((void *)_filter);
            #
            #     std::ostringstream shift;
            #     std::ostringstream sat;
            #
            #     shift << ext->get_param_int("shift");
            #     sat << ext->get_param_float("sat");
            #
            #     _filter = g_strdup_printf(
            #         "<filter xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" style=\"color-interpolation-filters:sRGB;\" inkscape:label=\"Color Shift\">\n"
            #           "<feColorMatrix type=\"hueRotate\" values=\"%s\" result=\"color1\" />\n"
            #           "<feColorMatrix type=\"saturate\" values=\"%s\" result=\"color2\" />\n"
            #         "</filter>\n", shift.str().c_str(), sat.str().c_str() );
            #
            #     return _filter;
            # }; /* ColorShift filter */

            if not (self.options.shift_shift == 0 and
                    self.options.shift_sat == 1.0):

                # 1. feColorMatrix: Shift
                r1, g1, b1 = cm_hueRotate(r, g, b, self.options.shift_shift)

                # 2. feColorMatrix: Saturate
                r2, g2, b2 = cm_saturate(r1, g1, b1, self.options.shift_sat)

                # if all is done
                r_new, g_new, b_new = r2, g2, b2

        else:   # catch remaining tabs and options on main page

            # 'About' tab, or unknown tab
            pass

        # return RGB hex string for new color values:
        return '%02x%02x%02x' % (r_new, g_new, b_new)


c = C()
c.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
