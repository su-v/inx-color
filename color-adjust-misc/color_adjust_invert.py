#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: Adjust Invert
"""

# local library
from colormatrix import *
import coloreffect
import inkex


class C(coloreffect.ColorEffect):
    """
    Inkscape Coloreffect extension module
    """
    def __init__(self):
        coloreffect.ColorEffect.__init__(self)
        self.OptionParser.add_option("--invert_channels",
                                     action="store", type="string",
                                     dest="invert_channels", default="0",
                                     help="Invert channels")
#        self.OptionParser.add_option("--invert_opacify",
#                                     action="store", type="float",
#                                     dest="invert_opacify", default="0.",
#                                     help="Light transparency")
        self.OptionParser.add_option("--invert_hue",
                                     action="store", type="inkbool",
                                     dest="invert_hue", default=False,
                                     help="Invert hue")
        self.OptionParser.add_option("--invert_lightness",
                                     action="store", type="inkbool",
                                     dest="invert_lightness", default=False,
                                     help="Invert lightness")
#        self.OptionParser.add_option("--invert_transparency",
#                                     action="store", type="inkbool",
#                                     dest="invert_transparency", default=False,
#                                     help="Invert transparency")
        self.OptionParser.add_option("--rot_hue_channels",
                                     action="store", type="string",
                                     dest="rot_hue_channels", default="0",
                                     help="Invert channels")
        self.OptionParser.add_option("--rot_hue_rotate",
                                     action="store", type="float",
                                     dest="rot_hue_rotate", default="0.",
                                     help="Hue Rotate")
#        self.OptionParser.add_option("--rot_hue_inv",
#                                     action="store", type="inkbool",
#                                     dest="inv_hue_inv", default=False,
#                                     help="Invert hue")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab when OK was pressed")

    def colmod(self, r, g, b):
        """
        Parse options (depending on active tab) and transform color values
        Return RGB string (hex)
        """
        # assign initial new value (identity):
        r_new, g_new, b_new = r, g, b

        if self.options.tab == '"invert_tab"':

            # based on Inkscape 0.49 CPF 'Filters > Invert'

            # /**
            #     \brief    Custom predefined Invert filter.
            #
            #     Manage hue, lightness and transparency inversions
            #
            #     Filter's parameters:
            #     * Invert hue (boolean, default false) -> color1 (values, true: 180, false: 0)
            #     * Invert lightness (boolean, default false) -> color1 (values, true: 180, false: 0; XOR with Invert hue),
            #                                                    color2 (values: from a00 to a22, if 1, set -1 and set 1 in ax4, if -1, set 1 and set 0 in ax4)
            #     * Invert transparency (boolean, default false) -> color2 (values: negate a30, a31 and a32, substract 1 from a33)
            #     * Invert channels (enum, default Red and blue) -> color2 (values -for R&B: swap ax0 and ax2 in the first 3 lines)
            #     * Light transparency (0.->1., default 0.) -> color2 (values: a33=a33-x)
            # */

            # gchar const *
            # Invert::get_filter_text (Inkscape::Extension::Extension * ext)
            # {
            #     if (_filter != NULL) g_free((void *)_filter);
            #
            #     std::ostringstream line1;
            #     std::ostringstream line2;
            #     std::ostringstream line3;
            #
            #     std::ostringstream col5;
            #     std::ostringstream transparency;
            #     std::ostringstream hue;
            #
            #     if (ext->get_param_bool("hue") xor ext->get_param_bool("lightness")) {
            #         hue << "<feColorMatrix type=\"hueRotate\" values=\"180\" result=\"color1\" />\n";
            #     } else {
            #         hue << "";
            #     }
            #
            #     if (ext->get_param_bool("transparency")) {
            #         transparency << "0.21 0.72 0.07 " << 1 - ext->get_param_float("opacify");
            #     } else {
            #         transparency << "-0.21 -0.72 -0.07 " << 2 - ext->get_param_float("opacify");
            #     }
            #
            #     if (ext->get_param_bool("lightness")) {
            #         switch (atoi(ext->get_param_enum("channels"))) {
            #             case 1:
            #                 line1 << "0 0 -1";
            #                 line2 << "0 -1 0";
            #                 line3 << "-1 0 0";
            #                 break;
            #             case 2:
            #                 line1 << "0 -1 0";
            #                 line2 << "-1 0 0";
            #                 line3 << "0 0 -1";
            #                 break;
            #             case 3:
            #                 line1 << "-1 0 0";
            #                 line2 << "0 0 -1";
            #                 line3 << "0 -1 0";
            #                 break;
            #             default:
            #                 line1 << "-1 0 0";
            #                 line2 << "0 -1 0";
            #                 line3 << "0 0 -1";
            #                 break;
            #         }
            #         col5 << "1";
            #     } else {
            #         switch (atoi(ext->get_param_enum("channels"))) {
            #             case 1:
            #                 line1 << "0 0 1";
            #                 line2 << "0 1 0";
            #                 line3 << "1 0 0";
            #                 break;
            #             case 2:
            #                 line1 << "0 1 0";
            #                 line2 << "1 0 0";
            #                 line3 << "0 0 1";
            #                 break;
            #             case 3:
            #                 line1 << "1 0 0";
            #                 line2 << "0 0 1";
            #                 line3 << "0 1 0";
            #                 break;
            #             default:
            #                 line1 << "1 0 0";
            #                 line2 << "0 1 0";
            #                 line3 << "0 0 1";
            #                 break;
            #         }
            #         col5 << "0";
            #     }
            #
            #     _filter = g_strdup_printf(
            #         "<filter xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" style=\"color-interpolation-filters:sRGB;\" inkscape:label=\"Invert\">\n"
            #           "%s"
            #           "<feColorMatrix values=\"%s 0 %s %s 0 %s %s 0 %s %s 0 \" result=\"color2\" />\n"
            #         "</filter>\n", hue.str().c_str(),
            #                        line1.str().c_str(), col5.str().c_str(),
            #                        line2.str().c_str(), col5.str().c_str(),
            #                        line3.str().c_str(), col5.str().c_str(),
            #                        transparency.str().c_str() );
            #
            #     return _filter;
            # }; /* Invert filter */

            if True:

                # not supported via coloreffect.py
                self.options.invert_opacify = 0.0
                self.options.invert_transparency = False

                # fixed amount for hue rotation
                self.options.inv_hue_rotate = 180.0

                ## first feColorMatrix filter primitive, rotate hue

                if self.options.invert_hue != self.options.invert_lightness:
                    # feColorMatrix
                    r1, g1, b1 = cm_hueRotate(r, g, b, self.options.inv_hue_rotate)
                else:
                    r1, g1, b1 = r, g, b

                ## second feColorMatrix filter primitive

                invert_matrix2 = [[0 for row in range(5)] for col in range(5)]
                for i in (3, 4):
                    invert_matrix2[i][i] = 1

                # 4th row
                if self.options.invert_transparency:
                    #         transparency << "0.21 0.72 0.07 " << 1 - ext->get_param_float("opacify");
                    trans_1 = 0.21
                    trans_2 = 0.72
                    trans_3 = 0.07
                    trans_4 = 1 - self.options.invert_opacify
                else:
                    #         transparency << "-0.21 -0.72 -0.07 " << 2 - ext->get_param_float("opacify");
                    trans_1 = -0.21
                    trans_2 = -0.72
                    trans_3 = -0.07
                    trans_4 = 2 - self.options.invert_opacify

                invert_matrix2[3][0] = trans_1
                invert_matrix2[3][1] = trans_2
                invert_matrix2[3][2] = trans_3
                invert_matrix2[3][3] = trans_4

                if self.options.invert_lightness:
                    light_123 = -1
                    light_5 = 1
                else:
                    light_123 = 1
                    light_5 = 0

                # col1, col2, col3
                if self.options.invert_channels == "0":
                    # no inversion
                    invert_matrix2[0][0] = light_123
                    invert_matrix2[1][1] = light_123
                    invert_matrix2[2][2] = light_123
                elif self.options.invert_channels == "1":
                    # inversion: red and blue
                    invert_matrix2[0][2] = light_123
                    invert_matrix2[1][1] = light_123
                    invert_matrix2[2][0] = light_123
                elif self.options.invert_channels == "2":
                    # inversion: red and green
                    invert_matrix2[0][1] = light_123
                    invert_matrix2[1][0] = light_123
                    invert_matrix2[2][2] = light_123
                elif self.options.invert_channels == "3":
                    # inversion: green and blue
                    invert_matrix2[0][0] = light_123
                    invert_matrix2[1][2] = light_123
                    invert_matrix2[2][1] = light_123

                # col5
                for i in range(3):
                    invert_matrix2[i][4] = light_5

                #inkex.debug(invert_matrix2)

                # color as matrix
                rgba_matrix = rgba2matrix(r1, g1, b1, 1)

                # multiply the two matrices
                rgba_matrix_new = matrixmult(invert_matrix2, rgba_matrix)

                # matrix to color
                if rgba_matrix_new:
                    r2, g2, b2 = matrix2rgb(rgba_matrix_new)
                else:
                    # silently fail (matrix multiplication likely failed)
                    r2, g2, b2 = r1, g1, b1

                # return
                r_new, g_new, b_new = r2, g2, b2

        elif self.options.tab == '"rot_hue_tab"':

            # based on feColorMatrix > HueRotate

            if not (self.options.rot_hue_rotate == 0.0 and
                    self.options.rot_hue_rotate == 360.0):

                r_new, g_new, b_new = cm_hueRotate(r, g, b, self.options.rot_hue_rotate)

        else:   # catch remaining tabs and options on main page

            # 'About' tab, or unknown tab
            pass

        # return RGB hex string for new color values:
        return '%02x%02x%02x' % (r_new, g_new, b_new)


c = C()
c.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
