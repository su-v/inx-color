#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: Adjust Lightness-Contrast
"""

# local library
from colormatrix import *
import coloreffect
import inkex


class C(coloreffect.ColorEffect):
    """
    Inkscape Coloreffect extension module
    """
    def __init__(self):
        coloreffect.ColorEffect.__init__(self)
        self.OptionParser.add_option("--li_co_lightness",
                                     action="store", type="float",
                                     dest="li_co_lightness", default="0",
                                     help="Adjust RGB Lightness")
        self.OptionParser.add_option("--li_co_contrast",
                                     action="store", type="float",
                                     dest="li_co_contrast", default="0",
                                     help="Adjust RGB contrast")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab when OK was pressed")

    def colmod(self, r, g, b):
        """
        Parse options (depending on active tab) and transform color values
        Return RGB string (hex)
        """
        # assign initial new value (identity):
        r_new, g_new, b_new = r, g, b

        if self.options.tab == '"lightness_contrast_tab"':

            # based on Inkscape 0.49 CPF 'Filters > Color > Lightness-Contrast'

            # /**
            #     \brief    Custom predefined Lightness-Contrast filter.
            #
            #     Modify lightness and contrast separately.
            #
            #     Filter's parameters:
            #     * Lightness (0.->100., default 0.) -> colorMatrix
            #     * Contrast (0.->100., default 0.) -> colorMatrix
            #
            #     Matrix:
            #       Co/10 0     0     1+(Co-1)*Li/2000  -(Co-1)/20
            #       0     Co/10 0     1+(Co-1)*Li/2000  -(Co-1)/20
            #       0     0     Co/10 1+(Co-1)*Li/2000  -(Co-1)/20
            #       0     0     0     1                 0
            # */

            # gchar const *
            # LightnessContrast::get_filter_text (Inkscape::Extension::Extension * ext)
            # {
            #     if (_filter != NULL) g_free((void *)_filter);
            #
            #     std::ostringstream lightness;
            #     std::ostringstream contrast;
            #     std::ostringstream contrast5;
            #
            #     gfloat c5;
            #     if (ext->get_param_float("contrast") > 0) {
            #         contrast << (1 + ext->get_param_float("contrast") / 10);
            #         c5 = (- ext->get_param_float("contrast") / 20);
            #     } else {
            #         contrast << (1 + ext->get_param_float("contrast") / 100);
            #         c5 =(- ext->get_param_float("contrast") / 200);
            #     }
            #
            #     contrast5 << c5;
            #     lightness << ((1 - c5) * ext->get_param_float("lightness") / 100);
            #
            #     _filter = g_strdup_printf(
            #         "<filter xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" style=\"color-interpolation-filters:sRGB;\" inkscape:label=\"Lightness-Contrast\">\n"
            #           "<feColorMatrix values=\"%s 0 0 %s %s 0 %s 0 %s %s 0 0 %s %s %s 0 0 0 1 0\" />\n"
            #         "</filter>\n", contrast.str().c_str(), lightness.str().c_str(), contrast5.str().c_str(),
            #                        contrast.str().c_str(), lightness.str().c_str(), contrast5.str().c_str(),
            #                        contrast.str().c_str(), lightness.str().c_str(), contrast5.str().c_str() );
            #
            #     return _filter;
            # }; /* Lightness-Contrast filter */

            if not (self.options.li_co_contrast == 0.0 and
                    self.options.li_co_lightness == 0.0):

                # adjust contrast
                if self.options.li_co_contrast > 0:
                    contrast = 1 + self.options.li_co_contrast/10.0
                    c5 = (-1 * self.options.li_co_contrast)/20.0
                else:
                    contrast = 1 + self.options.li_co_contrast/100.0
                    c5 = (-1 * self.options.li_co_contrast)/200.0

                # adjust lightness (factors in contrast)
                lightness = (1 - c5)*self.options.li_co_lightness/100.0

                # color matrix based on contrast and lightness
                li_co_matrix = [[0 for row in range(5)] for col in range(5)]
                for i in range(3):
                    li_co_matrix[i][i] = contrast
                    li_co_matrix[i][3] = lightness
                    li_co_matrix[i][4] = c5
                for i in (3, 4):
                    li_co_matrix[i][i] = 1.0

                # color as matrix
                rgba_matrix = rgba2matrix(r, g, b, 1)

                # multiply the two matrices
                rgba_matrix_new = matrixmult(li_co_matrix, rgba_matrix)

                # return
                if rgba_matrix_new:
                    r_new, g_new, b_new = matrix2rgb(rgba_matrix_new)
                else:
                    # silently fail (matrix multiplication likely failed)
                    pass

        else:   # catch remaining tabs and options on main page

            # 'About' tab, or unknown tab
            pass

        # return RGB hex string for new color values:
        return '%02x%02x%02x' % (r_new, g_new, b_new)


c = C()
c.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
