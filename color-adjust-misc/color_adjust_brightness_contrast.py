#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: Adjust Brightness-Contrast
"""

# standard library
import colorsys
import math

# local library
import coloreffect
import inkex


def clamp(minimum, x, maximum):
    """
    Clamp value to min/max values
    """
    return max(minimum, min(x, maximum))


class C(coloreffect.ColorEffect):
    """
    Inkscape Coloreffect extension module
    """
    def __init__(self):
        coloreffect.ColorEffect.__init__(self)
        self.OptionParser.add_option("--br_brightness",
                                     action="store", type="int",
                                     dest="br_brightness", default="0",
                                     help="Adjust RGB Brightness")
        self.OptionParser.add_option("--co_contrast",
                                     action="store", type="int",
                                     dest="co_contrast", default="0",
                                     help="Adjust RGB contrast")
        self.OptionParser.add_option("--br_co_brightness",
                                     action="store", type="int",
                                     dest="br_co_brightness", default="0",
                                     help="Adjust RGB Brightness")
        self.OptionParser.add_option("--br_co_contrast",
                                     action="store", type="int",
                                     dest="br_co_contrast", default="0",
                                     help="Adjust RGB contrast")
        self.OptionParser.add_option("--br_co_brightness_gimp",
                                     action="store", type="int",
                                     dest="br_co_brightness_gimp", default="0",
                                     help="Adjust RGB Brightness (LUT)")
        self.OptionParser.add_option("--br_co_contrast_gimp",
                                     action="store", type="int",
                                     dest="br_co_contrast_gimp", default="0",
                                     help="Adjust RGB contrast (LUT)")
        self.OptionParser.add_option("--co_br_contrast_gegl",
                                     action="store", type="float",
                                     dest="co_br_contrast_gegl", default="0",
                                     help="Adjust RGB contrast (GEGL)")
        self.OptionParser.add_option("--co_br_brightness_gegl",
                                     action="store", type="float",
                                     dest="co_br_brightness_gegl", default="0",
                                     help="Adjust RGB Brightness (GEGL)")
        self.OptionParser.add_option("--br_co_brightness_pixastic",
                                     action="store", type="int",
                                     dest="br_co_brightness_pixastic", default="0",
                                     help="Adjust RGB Brightness (Pixastic)")
        self.OptionParser.add_option("--br_co_contrast_pixastic",
                                     action="store", type="float",
                                     dest="br_co_contrast_pixastic", default="0",
                                     help="Adjust RGB contrast (Pixastic)")
        self.OptionParser.add_option("--br_co_legacy_pixastic",
                                     action="store", type="inkbool",
                                     dest="br_co_legacy_pixastic", default=False,
                                     help="Legacy mode (Pixastic)")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab when OK was pressed")

    def colmod(self, r, g, b):
        """
        Parse options (depending on active tab) and transform color values
        Return RGB string (hex)
        """
        # assign initial new value (identity):
        r_new, g_new, b_new = r, g, b

        if self.options.tab == '"brightness_tab"':

            # Brightness calculation is based on Inkscape modules for
            # 'Extensions > Brighter', 'Extensions > Darker'
            # <http://bazaar.launchpad.net/~inkscape.dev/inkscape/trunk/view/head:/share/extensions/color_brighter.py>
            # <http://bazaar.launchpad.net/~inkscape.dev/inkscape/trunk/view/head:/share/extensions/color_darker.py>
            #
            # Description of original version (fixed factor 0.9):
            #
            # "Brightens the color(s) by dividing each RGB channel by 0.9. Channels with a value
            # of 0 remain unaffected. Although brighter and darker are inverse operations, the
            # results of a series of invocations will cause inconsistencies due to rounding errors.
            # Use More Light if you want to brighten up black areas as well."
            # <http://wiki.inkscape.org/wiki/index.php/Effect_reference#Brighter>
            #
            # "Darkens the color(s) by multiplying each RGB channel with 0.9. Although brighter and
            # darker are inverse operations, the results of a series of invocations will cause
            # inconsistencies due to rounding errors. Less Light is usually a better option,
            # because it operates in the HSL color space."
            # <http://wiki.inkscape.org/wiki/index.php/Effect_reference#Darker>

            if self.options.br_brightness > 0:
                # brighter
                FACTOR = max(100 - self.options.br_brightness, 100.0/255)

                # return color divided by brightness factor
                r_new, g_new, b_new = [min(int(round((c / (FACTOR/100.0)))), 255)
                                       for c in [r, g, b]]

            elif self.options.br_brightness < 0:
                # darker
                FACTOR = 100 + self.options.br_brightness

                # return color multipled with brightness factor
                r_new, g_new, b_new = [int(round(max(c * (FACTOR/100.0), 0)))
                                       for c in [r, g, b]]

        elif self.options.tab == '"contrast_tab"':

            # Contrast calculation is based on
            #  <http://www.dfstudios.co.uk/articles/image-processing-algorithms-part-5/>
            #
            # <quote>
            # Translating the above formulas into pseudo-code would give something like this:
            #
            #    factor = (259 * (contrast + 255)) / (255 * (259 - contrast))
            #    colour = GetPixelColour(x, y)
            #    newRed   = Truncate(factor * (Red(colour)   - 128) + 128)
            #    newGreen = Truncate(factor * (Green(colour) - 128) + 128)
            #    newBlue  = Truncate(factor * (Blue(colour)  - 128) + 128)
            #    PutPixelColour(x, y) = RGB(newRed, newGreen, newBlue)
            #
            # </quote>

            if self.options.co_contrast != 0:
                # less/more contrast
                FACTOR = 255 * self.options.co_contrast
                i = (259.0*(FACTOR/100.0 + 255.0)) / (255.0*(259.0 - FACTOR/100.0))

                # return
                r_new, g_new, b_new = [clamp(0, int(round(i*(c-128) + 128)), 255)
                                       for c in [r, g, b]]

        elif self.options.tab == '"brightness_contrast_tab"':

            # r_new,g_new,b_new hold a copy of original r,g,b values
            # and are returned as new values at the end of colmod(r,b,g)

            # set brightness
            if self.options.br_co_brightness > 0:
                # brighter
                FACTOR = max(100 - self.options.br_co_brightness, 100.0/255)

                # return color divided by brightness factor
                r_new, g_new, b_new = [min(int(round((c / (FACTOR/100.0)))), 255)
                                       for c in [r_new, g_new, b_new]]

            elif self.options.br_co_brightness < 0:
                # darker
                FACTOR = 100 + self.options.br_co_brightness

                # return color multipled with brightness factor
                r_new, g_new, b_new = [int(round(max(c * (FACTOR/100.0), 0)))
                                       for c in [r_new, g_new, b_new]]

            # set contrast based on result of brightness adjustment
            if self.options.br_co_contrast != 0:
                # less/more contrast
                FACTOR = 255 * self.options.br_co_contrast
                i = (259.0*(FACTOR/100.0 + 255.0)) / (255.0*(259.0 - FACTOR/100.0))

                # return
                r_new, g_new, b_new = [clamp(0, int(round(i*(c-128) + 128)), 255)
                                       for c in [r_new, g_new, b_new]]

        elif self.options.tab == '"brightness_contrast_gimp_tab"':

            # based on GIMP 2.8.2 'Colors > Brightness-Contrast' (lut)
            #
            # brightness: default value: 0 (no change)
            # brightness: default range: min="-127" max="127"
            #
            # contrast: default value: 0 (no change)
            # contrast: default range: min="-127" max="127"

            if not (self.options.br_co_brightness_gimp == 0 and
                    self.options.br_co_contrast_gimp == 0):

                midpo = 0.5 * 255
                shift = (self.options.br_co_brightness_gimp/127.0)/2.0
                slant = math.tan((self.options.br_co_contrast_gimp/127.0 + 1) * math.atan(1))

                if shift < 0.0:
                    # decrease brightness
                    r_new, g_new, b_new = [c*(1 + shift)
                                           for c in [r_new, g_new, b_new]]
                elif shift > 0.0:
                    # increase brightness
                    r_new, g_new, b_new = [c + (255 - c)*shift
                                           for c in [r_new, g_new, b_new]]

                if self.options.br_co_contrast_gimp != 0:
                    # adjust contrast
                    r_new, g_new, b_new = [(c - midpo)*slant + midpo
                                           for c in [r_new, g_new, b_new]]

                # return
                r_new, g_new, b_new = [clamp(0, int(round(c)), 255)
                                       for c in [r_new, g_new, b_new]]

        elif self.options.tab == '"contrast_brightness_gegl_tab"':

            # based on GIMP 2.8.2 'Tool > Gegl Operations' (GEGL)
            #
            # contrast: default value: 1.0 (no change)
            # contrast: default range: min="-5.00" max="5.00"
            #
            # brightness: default value: 0.0 (no change)
            # brightness: default range: min="-3.00" max="3.00"

            if not (self.options.co_br_contrast_gegl == 1.0 and
                    self.options.co_br_brightness_gegl == 0.0):

                midpo = 0.5 * 255
                slant = self.options.co_br_contrast_gegl
                shift = self.options.co_br_brightness_gegl * 255

                # adjust contrast, brightness
                r_new, g_new, b_new = [clamp(0, int(round((c - midpo)*slant + shift + midpo)), 255)
                                       for c in [r_new, g_new, b_new]]

        elif self.options.tab == '"brightness_contrast_pixastic_tab"':

            # based on Pixastic Lib
            #
            # brightness (int)
            #       The brightness value used, -150 to 150.
            # contrast (float)
            #       The contrast value, -1 to infinity, where a negative value decreases contrast,
            #       zero does nothing and positive values increase contrast.
            # legacy (bool)
            #       If true, brightness will be adjusted the same way Photoshop used to prior to
            #       CS3 (now called "Legacy" in the B&C dialog). This means the brightness value
            #       is simply added to the pixel value, in effect shifting the entire histogram
            #       left or right.

            if not (self.options.br_co_brightness_pixastic == 0 and
                    self.options.br_co_contrast_pixastic == 0.0):

                brightness = self.options.br_co_brightness_pixastic
                contrast = self.options.br_co_contrast_pixastic
                legacy = self.options.br_co_legacy_pixastic

                if legacy:
                    brightness = min(150, max(-150, brightness))
                else:
                    brightMul = 1 + min(150, max(-150, brightness))/150.0

                contrast = max(0, contrast+1)

                if contrast != 1:
                    if legacy:
                        mul = contrast
                        add = (brightness - 128)*contrast + 128
                    else:
                        mul = brightMul * contrast
                        add = -contrast*128 + 128
                else:
                    if legacy:
                        mul = 1
                        add = brightness
                    else:
                        mul = brightMul
                        add = 0

                # TODO: investigate optimization (preformance):
                # variant 2: less code, more calculations
                #
                #if legacy:
                #    mul = contrast
                #    add = (brightness - 128)*contrast + 128
                #else:
                #    mul = brightMul * contrast
                #    add = -contrast*128 + 128

                #inkex.debug("brightness: %s" % brightness)
                #inkex.debug("contrast: %s" % contrast)
                #inkex.debug("brightMul: %s" % brightMul)
                #inkex.debug("add: %s" % add)
                #inkex.debug("mul: %s" % mul)

                # adjust contrast, brightness
                r_new, g_new, b_new = [clamp(0, int(round(c*mul + add)), 255)
                                       for c in [r_new, g_new, b_new]]

        else:   # catch remaining tabs and options on main page

            # 'About' tab, or unknown tab
            pass

        # return RGB hex string for new color values:
        return '%02x%02x%02x' % (r_new, g_new, b_new)


c = C()
c.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
