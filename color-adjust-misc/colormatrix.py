#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: colormatrix.py
- matrix helper functions
- hueRotate, saturate based on feColorMatrix SVG filter primitive

Copyright (C) 2013 ~suv <suv-sf@users.sourceforge.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

# standard library
from math import sin, cos, pi


def clamp(minimum, x, maximum):
    """
    Clamp value to min/max values
    """
    return max(minimum, min(x, maximum))


def rgba2matrix(r, g, b, a):
    """
    Represent RGB color with Alpha as matrix
    For color effects Alpha is always 1
    """
    rgba_matrix = [[0 for row in range(1)] for col in range(5)]
    for i in range(3):
        rgba_matrix[i][0] = (r, g, b)[i]/255.0
    for i in (3, 4):
        rgba_matrix[i][0] = 1.0

    return rgba_matrix


def matrix2rgb(rgba_matrix):
    """
    Return r, g, b, values from RGBA matrix
    """
    r, g, b = [clamp(0, int(round(rgba_matrix[i][0]*255.0)), 255) for i in range(3)]

    return (r, g, b)


def matrixmult(A, B):
    """
    Multiply matrix A * B
    based on <http://stackoverflow.com/a/10508133>
    """
    rows_A = len(A)
    cols_A = len(A[0])
    rows_B = len(B)
    cols_B = len(B[0])

    if cols_A != rows_B:
        inkex.debug("Cannot multiply the two matrices. Incorrect dimensions.")
        return

    # Create the result matrix
    # Dimensions would be rows_A x cols_B
    C = [[0 for row in range(cols_B)] for col in range(rows_A)]

    for i in range(rows_A):
        for j in range(cols_B):
            for k in range(cols_A):
                C[i][j] += A[i][k]*B[k][j]
    return C


def cm_hueRotate(r_in, g_in, b_in, rotate):
    """
    Hue Rotate: hue is shifted by specifying one number ('rotate' in °)
    Only RGB values are changed.
    """

    r_out, g_out, b_out = r_in, g_in, b_in

    # hue rotate (lightness: 180°)
    hueRotate = rotate * pi/180.0

    # feColorMatrix: Rotate Hue
    hueRotate_matrix = [[0 for row in range(5)] for col in range(5)]
    for i in (3, 4):
        hueRotate_matrix[i][i] = 1

    a00 = 0.213 + cos(hueRotate)*(0.787) + sin(hueRotate)*(-0.213)
    a10 = 0.213 + cos(hueRotate)*(-0.213) + sin(hueRotate)*(0.143)
    a20 = 0.213 + cos(hueRotate)*(-0.213) + sin(hueRotate)*(-0.787)

    a01 = 0.715 + cos(hueRotate)*(-0.715) + sin(hueRotate)*(-0.715)
    a11 = 0.715 + cos(hueRotate)*(0.285) + sin(hueRotate)*(0.140)
    a21 = 0.715 + cos(hueRotate)*(-0.715) + sin(hueRotate)*(0.715)

    a02 = 0.072 + cos(hueRotate)*(-0.072) + sin(hueRotate)*(0.928)
    a12 = 0.072 + cos(hueRotate)*(-0.072) + sin(hueRotate)*(-0.283)
    a22 = 0.072 + cos(hueRotate)*(0.928) + sin(hueRotate)*(0.072)

    hueRotate_matrix[0][0] = a00
    hueRotate_matrix[0][1] = a01
    hueRotate_matrix[0][2] = a02
    hueRotate_matrix[1][0] = a10
    hueRotate_matrix[1][1] = a11
    hueRotate_matrix[1][2] = a12
    hueRotate_matrix[2][0] = a20
    hueRotate_matrix[2][1] = a21
    hueRotate_matrix[2][2] = a22

    # multiply the two matrices
    rgba_matrix_new = matrixmult(hueRotate_matrix, rgba2matrix(r_in, g_in, b_in, 1))

    # return
    if rgba_matrix_new:
        r_out, g_out, b_out = matrix2rgb(rgba_matrix_new)
    else:
        # silently fail (matrix multiplication likely failed)
        pass

    return (r_out, g_out, b_out)


def cm_saturate(r_in, g_in, b_in, sat):
    """
    Reduce saturation by specifying one number, s (or 'sat').
    The range of s is 0.0 (completely desaturated) to 1.0 (unchanged).
    Only the RGB values are changed.
    """

    r_out, g_out, b_out = r_in, g_in, b_in

    # feColorMatrix: Saturate
    #
    #    | R' |     |0.213+0.787s  0.715-0.715s  0.072-0.072s 0  0 |   | R |
    #    | G' |     |0.213-0.213s  0.715+0.285s  0.072-0.072s 0  0 |   | G |
    #    | B' |  =  |0.213-0.213s  0.715-0.715s  0.072+0.928s 0  0 | * | B |
    #    | A' |     |           0            0             0  1  0 |   | A |
    #    | 1  |     |           0            0             0  0  1 |   | 1 |
    #

    sat_matrix = [[0 for row in range(5)] for col in range(5)]
    for i in (3, 4):
        sat_matrix[i][i] = 1

    a00 = 0.213 + sat*(0.787)
    a10 = 0.213 + sat*(-0.213)
    a20 = 0.213 + sat*(-0.213)

    a01 = 0.715 + sat*(-0.715)
    a11 = 0.715 + sat*(0.285)
    a21 = 0.715 + sat*(-0.715)

    a02 = 0.072 + sat*(-0.072)
    a12 = 0.072 + sat*(-0.072)
    a22 = 0.072 + sat*(0.928)

    sat_matrix[0][0] = a00
    sat_matrix[0][1] = a01
    sat_matrix[0][2] = a02
    sat_matrix[1][0] = a10
    sat_matrix[1][1] = a11
    sat_matrix[1][2] = a12
    sat_matrix[2][0] = a20
    sat_matrix[2][1] = a21
    sat_matrix[2][2] = a22

    # multiply the two matrices
    rgba_matrix_new = matrixmult(sat_matrix, rgba2matrix(r_in, g_in, b_in, 1))

    # return
    if rgba_matrix_new:
        r_out, g_out, b_out = matrix2rgb(rgba_matrix_new)
    else:
        # silently fail (matrix multiplication likely failed)
        pass

    return (r_out, g_out, b_out)


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
