#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: Sepia
"""

# local library
from colormatrix import *
import coloreffect
import inkex


class C(coloreffect.ColorEffect):
    """
    Inkscape Coloreffect extension module
    """
    def __init__(self):
        coloreffect.ColorEffect.__init__(self)
        self.OptionParser.add_option("--sepia_mode",
                                     action="store", type="string",
                                     dest="sepia_mode", default="microsoft",
                                     help="Sepia mode (default, fast, alternate)")
        self.OptionParser.add_option("--sepia_yiq_i",
                                     action="store", type="float",
                                     dest="sepia_yiq_i", default="0.2",
                                     help="YIQ: I channel")
        self.OptionParser.add_option("--sepia_yiq_q",
                                     action="store", type="float",
                                     dest="sepia_yiq_q", default="0.0",
                                     help="YIQ: Q channel")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab", default="'sepia_tab'",
                                     help="The selected UI-tab when OK was pressed")

    def colmod(self, r, g, b):
        """
        Parse options (depending on active tab) and transform color values
        Return RGB string (hex)
        """
        # assign initial new value (identity):
        r_new, g_new, b_new = r, g, b

        if self.options.tab == '"sepia_tab"':

            # based on:
            # <http://www.pixastic.com/lib/docs/actions/sepia/>

            # /*
            #  * Pixastic Lib - Sepia filter - v0.1.0
            #  * Copyright (c) 2008 Jacob Seidelin, jseidelin@nihilogic.dk, http://blog.nihilogic.dk/
            #  * License: [http://www.pixastic.com/lib/license.txt]
            #  */
            #
            # Pixastic.Actions.sepia = {
            #
            # 	process : function(params) {
            # 		var mode = (parseInt(params.options.mode,10)||0);
            # 		if (mode < 0) mode = 0;
            # 		if (mode > 1) mode = 1;
            #
            # 		if (Pixastic.Client.hasCanvasImageData()) {
            # 			var data = Pixastic.prepareData(params);
            # 			var rect = params.options.rect;
            # 			var w = rect.width;
            # 			var h = rect.height;
            # 			var w4 = w*4;
            # 			var y = h;
            # 			do {
            # 				var offsetY = (y-1)*w4;
            # 				var x = w;
            # 				do {
            # 					var offset = offsetY + (x-1)*4;
            #
            # 					if (mode) {
            # 						// a bit faster, but not as good
            # 						var d = data[offset] * 0.299 + data[offset+1] * 0.587 + data[offset+2] * 0.114;
            # 						var r = (d + 39);
            # 						var g = (d + 14);
            # 						var b = (d - 36);
            # 					} else {
            # 						// Microsoft
            # 						var or = data[offset];
            # 						var og = data[offset+1];
            # 						var ob = data[offset+2];
            #
            # 						var r = (or * 0.393 + og * 0.769 + ob * 0.189);
            # 						var g = (or * 0.349 + og * 0.686 + ob * 0.168);
            # 						var b = (or * 0.272 + og * 0.534 + ob * 0.131);
            # 					}
            #
            # 					if (r < 0) r = 0; if (r > 255) r = 255;
            # 					if (g < 0) g = 0; if (g > 255) g = 255;
            # 					if (b < 0) b = 0; if (b > 255) b = 255;
            #
            # 					data[offset] = r;
            # 					data[offset+1] = g;
            # 					data[offset+2] = b;
            #
            # 				} while (--x);
            # 			} while (--y);
            # 			return true;
            # 		}
            # 	},
            # 	checkSupport : function() {
            # 		return Pixastic.Client.hasCanvasImageData();
            # 	}
            # }

            if self.options.sepia_mode == "microsoft":

                # default, better
                r_new, g_new, b_new = [clamp(0, int(round(c)), 255)
                                       for c in self.sepia_microsoft(r, g, b)]

            elif self.options.sepia_mode == "fast":

                # faster
                r_new, g_new, b_new = [clamp(0, int(round(c)), 255)
                                       for c in self.sepia_fast(r, g, b)]

            elif self.options.sepia_mode == "alt":

                # alternate matrix from <http://stackoverflow.com/a/9709217>
                r_new, g_new, b_new = [clamp(0, int(round(c)), 255)
                                       for c in self.sepia_cm_alternate(r, g, b)]

            elif self.options.sepia_mode == "cm1":

                # *** disabled ***

                # matrix (Microsoft):
                #
                # var r = (or * 0.393 + og * 0.769 + ob * 0.189);
                # var g = (or * 0.349 + og * 0.686 + ob * 0.168);
                # var b = (or * 0.272 + og * 0.534 + ob * 0.131);

                sepia1_matrix = [[0 for row in range(5)] for col in range(5)]
                sepia1_matrix[0][0] = 0.393
                sepia1_matrix[0][1] = 0.769
                sepia1_matrix[0][2] = 0.189
                sepia1_matrix[1][0] = 0.349
                sepia1_matrix[1][1] = 0.686
                sepia1_matrix[1][2] = 0.168
                sepia1_matrix[2][0] = 0.272
                sepia1_matrix[2][1] = 0.534
                sepia1_matrix[2][2] = 0.131
                for i in (3, 4):
                    sepia1_matrix[i][i] = 1.0

                #inkex.debug(sepia1_matrix)

                # color as matrix
                rgba_matrix = rgba2matrix(r, g, b, 1)

                # multiply the two matrices
                rgba_matrix_new = matrixmult(sepia1_matrix, rgba_matrix)

                # return
                if rgba_matrix_new:
                    r_new, g_new, b_new = matrix2rgb(rgba_matrix_new)
                else:
                    # silently fail (matrix multiplication likely failed)
                    pass

            elif self.options.sepia_mode == "cm2":

                # *** disabled ***

                # matrix (stackoverflow):
                #
                #         {0.3588, 0.7044, 0.1368, 0},
                #         {0.2990, 0.5870, 0.1140, 0},
                #         {0.2392, 0.4696, 0.0912 ,0},
                #         {0,0,0,0},

                sepia2_matrix = [[0 for row in range(5)] for col in range(5)]
                sepia2_matrix[0][0] = 0.3588
                sepia2_matrix[0][1] = 0.7044
                sepia2_matrix[0][2] = 0.1368
                sepia2_matrix[1][0] = 0.2990
                sepia2_matrix[1][1] = 0.5870
                sepia2_matrix[1][2] = 0.1140
                sepia2_matrix[2][0] = 0.2392
                sepia2_matrix[2][1] = 0.4696
                sepia2_matrix[2][2] = 0.0912
                for i in (3, 4):
                    sepia2_matrix[i][i] = 1.0

                #inkex.debug(sepia2_matrix)

                # color as matrix
                rgba_matrix = rgba2matrix(r, g, b, 1)

                # multiply the two matrices
                rgba_matrix_new = matrixmult(sepia2_matrix, rgba_matrix)

                # return
                if rgba_matrix_new:
                    r_new, g_new, b_new = matrix2rgb(rgba_matrix_new)
                else:
                    # silently fail (matrix multiplication likely failed)
                    pass

            elif self.options.sepia_mode == "cm3":

                # *** disabled ***

                # RGB -> YIQ -> RGB
                #
                # <http://content.gpwiki.org/index.php/D3DBook:Useful_Effect_Snippets>
                # <http://www.cs.vu.nl/~eliens/media/local/shader/sepia.phl>

                # color as matrix
                rgba_matrix = rgba2matrix(r, g, b, 1)

                # convert RGB to YIQ
                yiq_matrix = self.rgb2yiq(rgba_matrix)
                # convert YIQ color to sepia tone
                yiq_matrix[1][0] = 0.2
                yiq_matrix[2][0] = 0.0
                # convert YIQ to RGB
                rgba_matrix_new = self.yiq2rgb(yiq_matrix)

                # return
                if rgba_matrix_new:
                    r_new, g_new, b_new = matrix2rgb(rgba_matrix_new)
                else:
                    # silently fail (matrix multiplication likely failed)
                    pass

            elif self.options.sepia_mode == "cm4":

                # *** disabled ***

                # RGB -> YIQ -> RGB
                #
                # <http://en.wikipedia.org/wiki/YIQ#Formulas>

                # color as matrix
                rgba_matrix = rgba2matrix(r, g, b, 1)

                # convert RGB to YIQ
                yiq_matrix = self.rgb2yiq(rgba_matrix)
                # convert YIQ color to sepia tone
                yiq_matrix[1][0] = 0.0750
                yiq_matrix[2][0] = 0.0010
                # convert YIQ to RGB
                rgba_matrix_new = self.yiq2rgb(yiq_matrix)

                # return
                if rgba_matrix_new:
                    r_new, g_new, b_new = matrix2rgb(rgba_matrix_new)
                else:
                    # silently fail (matrix multiplication likely failed)
                    pass

            else:
                pass

        elif self.options.tab == '"sepia_yiq_tab"':

            # RGB -> YIQ; modify I,Q; YIQ -> RGB
            #
            # <http://en.wikipedia.org/wiki/YIQ#Formulas>
            # <http://content.gpwiki.org/index.php/D3DBook:Useful_Effect_Snippets>
            # <http://www.cs.vu.nl/~eliens/media/local/shader/sepia.phl>

            if True:

                yiq_i = self.options.sepia_yiq_i
                yiq_q = self.options.sepia_yiq_q

                # color as matrix
                rgba_matrix = rgba2matrix(r, g, b, 1)

                # convert RGB to YIQ
                yiq_matrix = self.rgb2yiq(rgba_matrix)
                # convert YIQ color to sepia tone
                yiq_matrix[1][0] = yiq_i
                yiq_matrix[2][0] = yiq_q
                # convert YIQ to RGB
                rgba_matrix_new = self.yiq2rgb(yiq_matrix)

                # return
                if rgba_matrix_new:
                    r_new, g_new, b_new = matrix2rgb(rgba_matrix_new)
                else:
                    # silently fail (matrix multiplication likely failed)
                    pass

        else:   # catch remaining tabs and options on main page

            # 'About' tab
            pass

        # return RGB hex string for new color values:
        return '%02x%02x%02x' % (r_new, g_new, b_new)

    def sepia_microsoft(self, r_in, g_in, b_in):
        """
        Default sepia function
        """
        r_out = r_in*0.393 + g_in*0.769 + b_in*0.189
        g_out = r_in*0.349 + g_in*0.686 + b_in*0.168
        b_out = r_in*0.272 + g_in*0.534 + b_in*0.131
        return r_out, g_out, b_out

    def sepia_fast(self, r, g, b):
        """
        Faster sepia function
        """
        d = r*0.299 + g*0.587 + b*0.114
        return d + 39, d + 14, d - 36

    def sepia_cm_alternate(self, r_in, g_in, b_in):
        """
        Alternate sepia function
        """
        # matrix from <http://stackoverflow.com/a/9709217>
        #
        #         {0.3588, 0.7044, 0.1368, 0},
        #         {0.2990, 0.5870, 0.1140, 0},
        #         {0.2392, 0.4696, 0.0912 ,0},
        #         {0,0,0,0},

        r_out = r_in*0.3588 + g_in*0.7044 + b_in*0.1368
        g_out = r_in*0.2990 + g_in*0.5870 + b_in*0.1140
        b_out = r_in*0.2392 + g_in*0.4696 + b_in*0.0912
        return r_out, g_out, b_out

    def rgb2yiq(self, rgba_matrix):
        """
        Convert RGB to YIQ
        """
        # RGB -> YIQ
        rgb2yiq_matrix = [[0 for row in range(5)] for col in range(5)]
        rgb2yiq_matrix[0][0] = 0.299
        rgb2yiq_matrix[0][1] = 0.587
        rgb2yiq_matrix[0][2] = 0.114
        rgb2yiq_matrix[1][0] = 0.595716
        rgb2yiq_matrix[1][1] = -0.274453
        rgb2yiq_matrix[1][2] = -0.321263
        rgb2yiq_matrix[2][0] = 0.211456
        rgb2yiq_matrix[2][1] = -0.522591
        rgb2yiq_matrix[2][2] = 0.311135
        for i in (3, 4):
            rgb2yiq_matrix[i][i] = 1.0
        # convert RGB to YIQ
        return matrixmult(rgb2yiq_matrix, rgba_matrix)

    def yiq2rgb(self, yiq_matrix):
        """
        Convert YIQ to RGB
        """
        # YIQ -> RGB
        yiq2rgb_matrix = [[0 for row in range(5)] for col in range(5)]
        yiq2rgb_matrix[0][0] = 1.0
        yiq2rgb_matrix[0][1] = 0.95568806036115671171
        yiq2rgb_matrix[0][2] = 0.61985809445637075388
        yiq2rgb_matrix[1][0] = 1.0
        yiq2rgb_matrix[1][1] = -0.27158179694405859326
        yiq2rgb_matrix[1][2] = -0.64687381613840131330
        yiq2rgb_matrix[2][0] = 1.0
        yiq2rgb_matrix[2][1] = -1.1081773266826619523
        yiq2rgb_matrix[2][2] = 1.7050645599191817149
        for i in (3, 4):
            yiq2rgb_matrix[i][i] = 1.0
        # convert YIQ to RGB
        return matrixmult(yiq2rgb_matrix, yiq_matrix)


c = C()
c.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
