#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: various contributions
"""

# standard library
import math

# local library
import coloreffect
import inkex


class C(coloreffect.ColorEffect):
    """
    Inkscape Coloreffect extension module
    """
    def __init__(self):
        coloreffect.ColorEffect.__init__(self)
        self.OptionParser.add_option("--loadus_mode",
                                     action="store", type="string",
                                     dest="loadus_mode", default="linearize",
                                     help="One of Loadus' Effects")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab", default="'loadus_tab'",
                                     help="The selected UI-tab when OK was pressed")

    def colmod(self, r, g, b):
        """
        Parse options (depending on active tab) and transform color values
        Return RGB string (hex)
        """
        # assign initial new value (identity):
        r_new, g_new, b_new = r, g, b

        if self.options.tab == '"loadus_tab"':

            # based on:
            # <http://www.inkscapeforum.com/viewtopic.php?f=11&t=9158&p=33947>

            if self.options.loadus_mode == "35mm":

                # 35mm Film

                l = (max(r,g,b)+min(r,g,b))/2
                
                l = float(l) / 255
                r_new = float(r) / 255
                g_new = float(g) / 255
                b_new = float(b) / 255
            
                RedCurve = float(6)
                GreenCurve = float(2)
                BlueCurve = float(1)
            
                r_new = (1 /(1 + math.exp(- RedCurve * (r_new - .5))) - (1 / (1 + math.exp(RedCurve / 2))))/(1 - 2 * (1 / (1 + math.exp(RedCurve / 2))))
                g_new = (1 /(1 + math.exp(- GreenCurve * (g_new - .5))) - (1 / (1 + math.exp(GreenCurve / 2))))/(1 - 2 * (1 / (1 + math.exp(GreenCurve / 2))))
                b_new = (1 /(1 + math.exp(- BlueCurve * (b_new - .5))) - (1 / (1 + math.exp(BlueCurve / 2))))/(1 - 2 * (1 / (1 + math.exp(BlueCurve / 2))))
            
                if l < 0.5:
                  r_new = (2 * l - 1) * (r_new - r_new * r_new) + r_new
                else:
                  r_new = (2 * l - 1) * (math.sqrt(r_new) - r_new) + r_new
            
                if l < 0.5:
                  g_new = (2 * l - 1) * (g_new - g_new * g_new) + g_new
                else:
                  g_new = (2 * l - 1) * (math.sqrt(g_new) - g_new) + g_new
            
                if l < 0.5:
                  b_new = (2 * l - 1) * (b_new - b_new * b_new) + b_new
                else:
                  b_new = (2 * l - 1) * (math.sqrt(b_new) - b_new) + b_new
            
                r_new = int(r_new * 255)
                g_new = int(g_new * 255)
                b_new = int(b_new * 255)

            elif self.options.loadus_mode == "linearize":

                # Linearize
                r_new = float(r) / 255
                g_new = float(g) / 255
                b_new = float(b) / 255

                r_new = pow(r_new, 0.45)
                g_new = pow(g_new, 0.45)
                b_new = pow(b_new, 0.45)

                r_new = int(r_new * 255)
                g_new = int(g_new * 255)
                b_new = int(b_new * 255)

            elif self.options.loadus_mode == "vintage":

                # Vintage I
                r_new = float(r) / 255
                g_new = float(g) / 255
                b_new = float(b) / 255

                r_new = r_new * 0.75 + g_new * 0.25
                g_new = g_new * 0.75 + b_new * 0.25
                b_new = b_new * 0.75 + b_new * 0.25

                r_new = int(r_new * 255)
                g_new = int(g_new * 255)
                b_new = int(b_new * 255)

            elif self.options.loadus_mode == "vintage2":

                # Vintage II
                r_new = float(r) / 255
                g_new = float(g) / 255
                b_new = float(b) / 255

                g_new = g_new * 0.90 + 0.10
                b_new = b_new * 0.60 + 0.20

                r_new = int(r_new * 255)
                g_new = int(g_new * 255)
                b_new = int(b_new * 255)

            else:
                pass

        elif self.options.tab == '"contribs_tab"':

            # other contributions
            pass

        else:   # catch remaining tabs and options on main page

            # 'About' tab
            pass

        # return RGB hex string for new color values:
        return '%02x%02x%02x' % (r_new, g_new, b_new)


c = C()
c.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
