#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: Solarize
"""

# local library
import coloreffect
import inkex


def clamp(minimum, x, maximum):
    """
    Clamp value to min/max values
    """
    return max(minimum, min(x, maximum))


class C(coloreffect.ColorEffect):
    """
    Inkscape Coloreffect extension module
    """
    def __init__(self):
        coloreffect.ColorEffect.__init__(self)
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab when OK was pressed")

    def colmod(self, r, g, b):
        """
        Parse options (depending on active tab) and transform color values
        Return RGB string (hex)
        """
        # assign initial new value (identity):
        r_new, g_new, b_new = r, g, b

        if self.options.tab == '"solarize_tab"':

            # /*
            #  * Pixastic Lib - Solarize filter - v0.1.0
            #  * Copyright (c) 2008 Jacob Seidelin, jseidelin@nihilogic.dk, http://blog.nihilogic.dk/
            #  * License: [http://www.pixastic.com/lib/license.txt]
            #  */
            #
            # Pixastic.Actions.solarize = {
            #
            # 	process : function(params) {
            # 		var useAverage = !!(params.options.average && params.options.average != "false");
            #
            # 		if (Pixastic.Client.hasCanvasImageData()) {
            # 			var data = Pixastic.prepareData(params);
            # 			var rect = params.options.rect;
            # 			var w = rect.width;
            # 			var h = rect.height;
            # 			var w4 = w*4;
            # 			var y = h;
            # 			do {
            # 				var offsetY = (y-1)*w4;
            # 				var x = w;
            # 				do {
            # 					var offset = offsetY + (x-1)*4;
            #
            # 					var r = data[offset];
            # 					var g = data[offset+1];
            # 					var b = data[offset+2];
            #
            # 					if (r > 127) r = 255 - r;
            # 					if (g > 127) g = 255 - g;
            # 					if (b > 127) b = 255 - b;
            #
            # 					data[offset] = r;
            # 					data[offset+1] = g;
            # 					data[offset+2] = b;
            #
            # 				} while (--x);
            # 			} while (--y);
            # 			return true;
            # 		}
            # 	},
            # 	checkSupport : function() {
            # 		return (Pixastic.Client.hasCanvasImageData());
            # 	}
            # }

            pass

        else:   # catch remaining tabs and options on main page

            # 'About' tab

            # apply solarize effect (no parameters)
            r_new, g_new, b_new = [clamp(0, self.solarize(c), 255)
                                   for c in [r, g, b]]

        # return RGB hex string for new color values:
        return '%02x%02x%02x' % (r_new, g_new, b_new)

    def solarize(self, c):
        """
        Apply minimal solarize effect to channel
        """
        if c > 127:
            return 255 - c
        else:
            return c


c = C()
c.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
