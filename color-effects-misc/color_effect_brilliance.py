#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: Effect Brilliance
"""

# local library
from colormatrix import *
import coloreffect
import inkex


class C(coloreffect.ColorEffect):
    """
    Inkscape Coloreffect extension module
    """
    def __init__(self):
        coloreffect.ColorEffect.__init__(self)
        self.OptionParser.add_option("--brill_brightness",
                                     action="store", type="float",
                                     dest="brill_brightness", default="2.0",
                                     help="Adjust Brightness")
        self.OptionParser.add_option("--brill_sat",
                                     action="store", type="float",
                                     dest="brill_sat", default="0.5",
                                     help="Adjust Saturation")
        self.OptionParser.add_option("--brill_lightness",
                                     action="store", type="float",
                                     dest="brill_lightness", default="0.0",
                                     help="Adjust Lightness")
        self.OptionParser.add_option("--brill_invert",
                                     action="store", type="inkbool",
                                     dest="brill_invert", default=False,
                                     help="Invert Brilliance")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab when OK was pressed")

    def colmod(self, r, g, b):
        """
        Parse options (depending on active tab) and transform color values
        Return RGB string (hex)
        """
        # assign initial new value (identity):
        r_new, g_new, b_new = r, g, b

        if self.options.tab == '"brilliance_tab"':

            # based on Inkscape 0.49 CPF 'Filters > Color > Brilliance'

            # /**
            #     \brief    Custom predefined Brilliance filter.
            #
            #     Brilliance filter.
            #
            #     Filter's parameters:
            #     * Brilliance (1.->10., default 2.) -> colorMatrix (RVB entries)
            #     * Over-saturation (0.->10., default 0.5) -> colorMatrix (6 other entries)
            #     * Lightness (-10.->10., default 0.) -> colorMatrix (last column)
            #     * Inverted (boolean, default false) -> colorMatrix
            #
            #     Matrix:
            #       St Vi Vi 0  Li
            #       Vi St Vi 0  Li
            #       Vi Vi St 0  Li
            #       0  0  0  1  0
            # */

            # gchar const *
            # Brilliance::get_filter_text (Inkscape::Extension::Extension * ext)
            # {
            #     if (_filter != NULL) g_free((void *)_filter);
            #
            #     std::ostringstream brightness;
            #     std::ostringstream sat;
            #     std::ostringstream lightness;
            #
            #     if (ext->get_param_bool("invert")) {
            #         brightness << -ext->get_param_float("brightness");
            #         sat << 1 + ext->get_param_float("sat");
            #         lightness << -ext->get_param_float("lightness");
            #     } else {
            #         brightness << ext->get_param_float("brightness");
            #         sat << -ext->get_param_float("sat");
            #         lightness << ext->get_param_float("lightness");
            #     }
            #
            #     _filter = g_strdup_printf(
            #         "<filter xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" style=\"color-interpolation-filters:sRGB;\" inkscape:label=\"Brilliance\">\n"
            #           "<feColorMatrix values=\"%s %s %s 0 %s %s %s %s 0 %s %s %s %s 0 %s 0 0 0 1 0 \" />\n"
            #         "</filter>\n", brightness.str().c_str(), sat.str().c_str(), sat.str().c_str(),
            #                        lightness.str().c_str(), sat.str().c_str(), brightness.str().c_str(),
            #                        sat.str().c_str(), lightness.str().c_str(), sat.str().c_str(),
            #                        sat.str().c_str(), brightness.str().c_str(), lightness.str().c_str() );
            #
            #     return _filter;
            # }; /* Brilliance filter */

            if not (self.options.brill_brightness == 1.0 and
                    self.options.brill_sat == 0.0 and
                    self.options.brill_lightness == 0.0 and
                    self.options.brill_invert is False):

                if self.options.brill_invert:
                    brightness = -1 * self.options.brill_brightness
                    sat = 1.0 + self.options.brill_sat
                    lightness = -1 * self.options.brill_lightness
                else:
                    brightness = self.options.brill_brightness
                    sat = -1 * self.options.brill_sat
                    lightness = self.options.brill_lightness

                brill_matrix = [[0 for row in range(5)] for col in range(5)]
                for i in range(3):
                    for j in range(3):
                        brill_matrix[i][j] = sat
                        brill_matrix[i][3] = 0
                        brill_matrix[i][4] = lightness
                for i in range(3):
                    brill_matrix[i][i] = brightness
                for i in (3, 4):
                    brill_matrix[i][i] = 1.0

                #inkex.debug(brill_matrix)

                # color as matrix
                rgba_matrix = rgba2matrix(r, g, b, 1)

                # multiply the two matrices
                rgba_matrix_new = matrixmult(brill_matrix, rgba_matrix)

                # return
                if rgba_matrix_new:
                    r_new, g_new, b_new = matrix2rgb(rgba_matrix_new)
                else:
                    # silently fail (matrix multiplication likely failed)
                    pass

        else:   # catch remaining tabs and options on main page

            # 'About' tab, or unknown tab
            pass

        # return RGB hex string for new color values:
        return '%02x%02x%02x' % (r_new, g_new, b_new)


c = C()
c.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
