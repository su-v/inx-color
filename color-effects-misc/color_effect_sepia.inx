<?xml version="1.0" encoding="UTF-8"?>
<!-- file: color_effect_sepia.inx -->
<!-- deps: color_effect_sepia.py  -->
<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">

  <_name>Sepia</_name>
  <id>su-v/org.inkscape.color.effect_sepia</id>

  <dependency type="executable" location="extensions">color_effect_sepia.py</dependency>
  <dependency type="executable" location="extensions">colormatrix.py</dependency>
  <dependency type="executable" location="extensions">coloreffect.py</dependency>
  <dependency type="executable" location="extensions">inkex.py</dependency>
  <dependency type="executable" location="extensions">simplestyle.py</dependency>

  <param name="tab" type="notebook">

    <!-- Sepia (based on Pixastic Lib - Sepia filter - v0.1.0) -->
    <page name="sepia_tab" _gui-text="Sepia">
      <param name="sepia_mode" gui-text="Mode:" type="optiongroup">
        <_option value="microsoft">Default</_option>
        <_option value="fast"     >Fast</_option>
        <_option value="alt"      >Alternate</_option>
<!--
        <_option value="cm1"      >ColorMatrix: Default</_option>
        <_option value="cm2"      >ColorMatrix: Alternate</_option>
        <_option value="cm3"      >ColorMatrix: YIQ (I=0.2 Q=0.0)</_option>
        <_option value="cm4"      >ColorMatrix: YIQ (I=0.075 Q=0.01)</_option>
-->
      </param>
    </page>

    <!-- Sepia (RGB -> YIQ -> RGB) -->
    <page name="sepia_yiq_tab" _gui-text="YIQ">
      <param name="sepia_yiq_i"    gui-text="I:"  type="float" appearance="full" min="-0.5957" max="0.5957" precision="4">0.0750</param>
      <param name="sepia_yiq_q"    gui-text="Q:"  type="float" appearance="full" min="-0.5226" max="0.5226" precision="4">0.0010</param>
      <param name="sepia_yiq_desc" type="description" xml:space="preserve">(I, Q: channels of YIQ color space)</param>
    </page>

    <!-- about -->
    <page name="about_tab" _gui-text="About">
      <_param name="about_sepia_head"  type="description" appearance="header">Sepia:</_param>
      <_param name="about_sepia_desc"  type="description" xml:space="preserve">This acts like a desaturation action, but instead of a regular grayscale it adds a tinting to create an effect similar to sepia toning. An alternative 'fast' mode is available which is a bit faster but arguably doesn't produce as good a result as the default mode.</_param>

    </page>

  </param>

  <effect needs-live-preview="true">
    <object-type>all</object-type>
    <effects-menu>
      <submenu _name="Color">
        <submenu _name="Effects"/>
      </submenu>
    </effects-menu>
  </effect>

  <script>
    <command reldir="extensions" interpreter="python">color_effect_sepia.py</command>
  </script>

  <options silent="true"></options>

</inkscape-extension>
