#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: Effect Posterize
"""

# local library
import coloreffect
import inkex


def clamp(minimum, x, maximum):
    """
    Clamp value to min/max values
    """
    return max(minimum, min(x, maximum))


class C(coloreffect.ColorEffect):
    """
    Inkscape Coloreffect extension module
    """
    def __init__(self):
        coloreffect.ColorEffect.__init__(self)
        self.OptionParser.add_option("--posterize_level",
                                     action="store", type="int",
                                     dest="posterize_level", default="6",
                                     help="Adjust Level for Posterize")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab when OK was pressed")

    def colmod(self, r, g, b):
        """
        Parse options (depending on active tab) and transform color values
        Return RGB string (hex)
        """
        # assign initial new value (identity):
        r_new, g_new, b_new = r, g, b

        if self.options.tab == '"posterize_tab"':

            if not (self.options.posterize_level == 0):

                # Note: all variables are truncated to 'int'
                numLevels = max(2, min(256, self.options.posterize_level))
                numAreas = 256 / numLevels
                numValues = 255 / (numLevels - 1)

                # adjust posterize (based on number of levels)
                r_new, g_new, b_new = [clamp(0, numValues * (c/numAreas), 255)
                                       for c in [r, g, b]]

        else:   # catch remaining tabs and options on main page

            # 'About' tab, or unknown tab
            pass

        # return RGB hex string for new color values:
        return '%02x%02x%02x' % (r_new, g_new, b_new)


c = C()
c.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
