#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
ColorEffect module: Effect Fade to Back or White
"""

# local library
from colormatrix import *
import coloreffect
import inkex


class C(coloreffect.ColorEffect):
    """
    Inkscape Coloreffect extension module
    """
    def __init__(self):
        coloreffect.ColorEffect.__init__(self)
        self.OptionParser.add_option("--fade2bw_level",
                                     action="store", type="float",
                                     dest="fade2bw_level", default="1.0",
                                     help="Fade level")
        self.OptionParser.add_option("--fade2bw_fadeto",
                                     action="store", type="string",
                                     dest="fade2bw_fadeto", default="black",
                                     help="Fade target")
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab when OK was pressed")

    def colmod(self, r, g, b):
        """
        Parse options (depending on active tab) and transform color values
        Return RGB string (hex)
        """
        # assign initial new value (identity):
        r_new, g_new, b_new = r, g, b

        if self.options.tab == '"fade2bw_tab"':

            # based on Inkscape 0.49 CPF 'Filters > Color > Brilliance'

            # /**
            #     \brief    Custom predefined Fade to Black or White filter.
            #
            #     Fade to black or white.
            #
            #     Filter's parameters:
            #     * Level (0.->1., default 1.) -> colorMatrix (RVB entries)
            #     * Fade to (enum [black|white], default black) -> colorMatrix (RVB entries)
            #
            #     Matrix
            #      black           white
            #     Lv 0  0  0 0    Lv 0  0  1-lv 0
            #     0  Lv 0  0 0    0  Lv 0  1-lv 0
            #     0  0  Lv 0 0    0  0  Lv 1-lv 0
            #     0  0  0  1 0    0  0  0  1    0
            # */

            # gchar const *
            # FadeToBW::get_filter_text (Inkscape::Extension::Extension * ext)
            # {
            #     if (_filter != NULL) g_free((void *)_filter);
            #
            #     std::ostringstream level;
            #     std::ostringstream wlevel;
            #
            #     level << ext->get_param_float("level");
            #
            #     const gchar *fadeto = ext->get_param_enum("fadeto");
            #     if ((g_ascii_strcasecmp("white", fadeto) == 0)) {
            #     // White
            #         wlevel << (1 - ext->get_param_float("level"));
            #     } else {
            #     // Black
            #         wlevel << "0";
            #     }
            #
            #     _filter = g_strdup_printf(
            #         "<filter xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\" style=\"color-interpolation-filters:sRGB;\" inkscape:label=\"Fade to Black or White\">\n"
            #           "<feColorMatrix values=\"%s 0 0 0 %s 0 %s 0 0 %s 0 0 %s 0 %s 0 0 0 1 0\" />\n"
            #         "</filter>\n", level.str().c_str(), wlevel.str().c_str(),
            #                        level.str().c_str(), wlevel.str().c_str(),
            #                        level.str().c_str(), wlevel.str().c_str() );
            #
            #     return _filter;
            # }; /* Fade to black or white filter */

            if not (self.options.fade2bw_level == 1.0):

                level = self.options.fade2bw_level

                if self.options.fade2bw_fadeto == "white":
                    wlevel = 1.0 - self.options.fade2bw_level
                else:   # self.options.fade2bw_fadeto == "black"
                    wlevel = 0.0

                fade2bw_matrix = [[0 for row in range(5)] for col in range(5)]
                for i in range(3):
                    fade2bw_matrix[i][i] = level
                    fade2bw_matrix[i][3] = wlevel
                    fade2bw_matrix[i][4] = 0
                for i in (3, 4):
                    fade2bw_matrix[i][i] = 1.0

                #inkex.debug(fade2bw_matrix)

                # color as matrix
                rgba_matrix = rgba2matrix(r, g, b, 1)

                # multiply the two matrices
                rgba_matrix_new = matrixmult(fade2bw_matrix, rgba_matrix)

                # return
                if rgba_matrix_new:
                    r_new, g_new, b_new = matrix2rgb(rgba_matrix_new)
                else:
                    # silently fail (matrix multiplication likely failed)
                    pass

        else:   # catch remaining tabs and options on main page

            # 'About' tab, or unknown tab
            pass

        # return RGB hex string for new color values:
        return '%02x%02x%02x' % (r_new, g_new, b_new)


c = C()
c.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
